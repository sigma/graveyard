format = unwritten-1

dev-python/
    backports_shutil_get_terminal_size/
        :0 1.0.0
            comment = Python2 only, nothing depends on it anymore
            commit-id = 2d4c559b
            description = A backport of the get_terminal_size function from Python 3.3's shutil
            homepage = https://pypi.org/project/backports.shutil_get_terminal_size/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    backports_shutil_which/
        :0 3.5.2
            comment = Python2 only, nothing depends on it anymore
            commit-id = 09c0652f
            description = Backport of shutil.which from Python 3.3
            homepage = https://pypi.org/project/backports.shutil_which/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    BeautifulSoup/
        :0 3.2.1
            comment = Python2 only, nothing depends on it anymore, replaced by beautifulsoup4
            commit-id = 38ca290e
            description = HTML/XML parser for quick-turnaround applications like screen-scraping
            homepage = http://www.crummy.com/software/BeautifulSoup/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    buildutils/
        :0 0.3-r1
            comment = Python2 only, nothing depends on it anymore
            commit-id = 0eff66ba
            description = Extensions for developing Python libraries and applications
            homepage = http://buildutils.lesscode.org/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    cef/
        :0 0.3
            comment = was unused, update to setup-py.exlib if you revive
            commit-id = d5ec396
            description = Module that emits CEF logs
            homepage = https://github.com/mozilla/cef
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    Imaging/
        :0 1.1.7-r3
            comment = Dead upstream, use dev-python/Pillow
            commit-id = 55d243bbbac03009a60cf93565442d23d633886f
            description = Python Imaging Library (PIL)
            homepage = http://www.pythonware.com/products/pil/index.htm
            removed-by = Wulf C. Krueger <philantrop@exherbo.org>
            removed-from = python
    iso3166/
        :0 2.0.2
            comment = No longer needed by anything
            commit-id = d4b50aa95
            description = Self-contained ISO 3166-1 country definitions
            homepage = https://pypi.org/project/iso3166/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    iso_639/
        :0 0.4.5
            comment = No longer needed by anything
            commit-id = 7f689e8f6
            description = Python library for ISO 639 standard
            homepage = https://github.com/noumar/iso639
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    letsencrypt/
        :0 0.5.0
            comment = letsencrypt got renamed to certbot
            commit-id = fe914c8
            description = Let's Encrypt client
            homepage = https://pypi.python.org/pypi/letsencrypt/
            removed-by = Arnaud Lefebvre <a.lefebvre@outlook.fr>
            removed-from = python
    logilab-common/
        :0 1.4.2
            comment = No longer needed by anything, problematic deps, broken tests
            commit-id = e6f9e3cad
            description = Collection of low-level Python packages and modules used by Logilab projects
            homepage = https://pypi.org/project/logilab-common/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    metlog-cef/
        :0 0.1
            comment = was unused, update to setup-py.exlib if you revive
            commit-id = fb71995
            description = CEF extensions to metrics logging
            homepage = https://github.com/mozilla-services/metlog-cef
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    metlog-py/
        :0 0.9.7
            comment = was unused, update to setup-py.exlib if you revive
            commit-id = db1a709
            description = Metrics Logging
            homepage = https://github.com/mozilla-services/metlog-py
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    msgpack-python/
        :0 0.4.8-r1
            comment = upstream renamed msgpack-python to msgpack
            commit-id = bba5ea8
            description = CPython bindings for reading and writing MessagePack data
            homepage = http://msgpack.org
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = python
    nosehtmloutput/
        :0 0.0.5
            comment = Nothing needs it anymore, depends on the unmaintained nose
            commit-id = aed638f3
            description = Nose plugin to produce test results in html
            homepage = https://pypi.org/project/nosehtmloutput/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    nosexcover/
        :0 1.0.10
            comment = Nothing needs it anymore, depends on the unmaintained nose
            commit-id = a0baaa65
            description = Fork of nose.plugins.cover that enables XML output
            homepage = https://pypi.org/project/nosexcover/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    openstack-nose/
        :0 0.11-r1
            comment = Nothing needs it anymore, depends on the unmaintained nose
            commit-id = c3dd77dc
            description = Openstack style output for nosetests
            homepage = https://github.com/openstack-dev/openstack-nose
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    Paste/
        :0 1.7.5.1-r1
            comment = was unused, update to setup-py.exlib if you revive
            commit-id = 8a604b4
            description = Tools for using a Web Server Gateway Interface stack
            homepage = https://pypi.python.org/pypi/Paste/
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    PasteDeploy/
        :0 1.5.0-r1
            comment = was unused, update to setup-py.exlib if you revive
            commit-id = f41df2a
            description = Load, configure, and compose WSGI applications and servers
            homepage = https://pypi.python.org/pypi/PasteDeploy/
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    PasteScript/
        :0 1.7.4.2
            comment = was unused, update to setup-py.exlib if you revive
            commit-id = b9810b9
            description = A pluggable command-line frontend, including commands to setup package file layouts
            homepage = http://pythonpaste.org/script/
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    pudge/
        :0 0.1.3-r1
            comment = Python2 only, nothing depends on it anymore
            commit-id = f107ec65
            description = A documentation generator for Python projects, using Restructured Textbuildutils
            homepage = http://pudge.lesscode.org
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    pycryptopp/
        :0 0.6.0.1206569328141510525648634803928199668821045408958
            comment = Python2 only, nothing depends on it anymore
            commit-id = 09f22428
            description = Python wrappers for a few algorithms from the Crypto++ library
            homepage = https://pypi.org/project/pycryptopp/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    pylibmc/
        :0 1.2.3
            comment = was unused, update to setup-py.exlib if you revive
            commit-id = 7ecce79
            description = Quick and small memcached client for Python
            homepage = http://sendapatch.se/projects/pylibmc/
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    PyQt4/
        :0 4.12-r2
            comment = Depends on the unmaintained and removed Qt4
            commit-id = f64ddd36
            description = PyQt4 is a set of Python bindings for the Qt4 toolkit
            homepage = http://www.riverbankcomputing.co.uk/software/pyqt/intro/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    Pyrex/
        :0 0.9.9
            comment = Dead upstream
            commit-id = 042143c
            description = A language for writing Python extension modules
            homepage = http://www.cosc.canterbury.ac.nz/greg.ewing/python/Pyrex
            removed-by = Kylie McClain <somasis@exherbo.org>
            removed-from = python
    PySide/
        :0 1.2.2
            comment = Qt4 is dead upstream, therefore PySide is too
            commit-id = 658b6584
            description = Python bindings for the Qt4 cross-platform application and UI framework
            homepage = https://pypi.org/project/PySide/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    pystache/
        :0 0.5.4 0.6.0
            comment = Nothing in our repos needed it anymore
            commit-id = c4ff45225
            description = Mustache in Python
            homepage = https://pypi.org/project/pystache/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    pysvn/
        :0 1.8.0
            comment = Unmaintained
            commit-id = 7a0eba5e
            description = The pysvn project's goal is to enable Tools to be written in Python that use Subversion.
            homepage = http://pysvn.tigris.org/
            removed-by = Marvin Schmidt <marv@exherbo.org>
            removed-from = python
    PySyck/
        :0 0.61.2
            comment = syck is dead upstream, therefore pysyck is too. please don't use it.
            commit-id = f7666f0
            description = Python bindings for the Syck YAML parser and emitter
            homepage = http://pyyaml.org/wiki/PySyck
            removed-by = Kylie McClain <somasis@exherbo.org>
            removed-from = python
    python-keystoneclient/
        :0 0.1.2
            comment = doesn't resolve, not bumped in ages. Probably unused
            commit-id = 6fece88e0559c375a9cd3e3aff8b6150d32d5398
            description = Client library for OpenStack Keystone API
            homepage = http://pypi.python.org/pypi/python-keystoneclient/
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    python-ldap/
        :0 2.4.10
            comment = was unused, update to setup-py.exlib if you revive
            commit-id = c615899
            description = LDAP client library for Python, using OpenLDAP
            homepage = http://www.python-ldap.org/
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    python-swiftclient/
        :0 1.2.0-r1
            comment = Python2 only, outdated
            commit-id = 0a84263f
            description = python client for the Swift API (OpenStack Object Storage)
            homepage = https://pypi.org/project/python-swiftclient/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    python2-pythondialog/
        :0 3.4.0
            comment = Python2 only, nothing depends on it anymore
            commit-id = 84cfce45
            description = Python interface to the UNIX dialog utility and mostly-compatible programs
            homepage = https://pypi.org/project/python2-pythondialog/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    PyXML/
        :0 0.8.4-r1
            comment = Breaks Python tests and dead upstream
            commit-id = 078013ad8bc2bdf4eb3e7d54cd39d7f95090d5d1
            description = A collection of libraries to process XML with Python
            homepage = http://pyxml.sourceforge.net/
            removed-by = Kim Højgaard-Hansen <kimrhh@exherbo.org>
            removed-from = python
    recaptcha-client/
        :0 1.0.6
            comment = was unused, update to setup-py.exlib if you revive
            commit-id = 99d1973
            description = A plugin for reCAPTCHA and reCAPTCHA Mailhide
            homepage = http://recaptcha.net/
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    Routes/
        :0 1.12.3 1.13-r1
            comment = was unused, update to setup-py.exlib if you revive
            commit-id = 23c8d49
            description = Routing Recognition and Generation Tools
            homepage = https://pypi.python.org/pypi/Routes/
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    sancho/
        :0 0.11-r1
            comment = Python2 only, nothing depends on it anymore
            commit-id = 90277bb2
            description = Sancho is a unit testing framework
            homepage = https://pypi.org/project/Sancho/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    scrypt/
        :0 0.6.1
            comment = was unused, update to setup-py.exlib if you revive
            commit-id = 3c2332e
            description = Bindings for the scrypt key derivation function library
            homepage = http://bitbucket.org/mhallin/py-scrypt
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    smmap2/
        :0 2.0.5
            comment = Mirrror package for smmap, just install that
            commit-id = a25266e58
            description = A pure Python implementation of a sliding memory map
            homepage = https://pypi.org/project/smmap2/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = python
    txacme/
        :0 0.9.3
            comment = Broken as it supports ACMEv1 only, now unused
            commit-id = 8f850f1
            description = ACME protocol implementation for Twisted
            homepage = https://pypi.org/project/txacme/
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = python
    WSGIProxy/
        :0 0.2.2
            comment = was unused, update to setup-py.exlib if you revive
            commit-id = 18a5e62
            description = HTTP proxying tools for WSGI apps
            homepage = http://pythonpaste.org/wsgiproxy/
            removed-by = Benedikt Morbach <moben@exherbo.org>
            removed-from = python
    ZSI/
        :0 2.0-r1
            comment = dead upstream for approximately five thousand years
            commit-id = 30b6391
            description = Python package that provides an implementation of the SOAP specification
            homepage = http://pywebsvcs.sourceforge.net/zsi.html
            removed-by = Kylie McClain <somasis@exherbo.org>
            removed-from = python
